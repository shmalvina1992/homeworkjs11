"use strict"

const form = document.querySelector('.password-form');
const icons = document.querySelectorAll('i');
const inputUp = document.getElementsByTagName('input')[0];
const inputDown = document.getElementsByTagName('input')[1];
const button = document.querySelector('button');
const paragraph = document.createElement('p');

form.addEventListener('click', e => {
    e.preventDefault();
    
    icons.forEach(item => {
        if (e.target === item) {
            item.classList.toggle('fa-eye-slash');

            if (item.previousElementSibling.getAttribute('type') === 'password') {
                item.previousElementSibling.setAttribute('type', 'text');   
            } else {
                item.previousElementSibling.setAttribute('type', 'password')
            }
        }
    })

    if (e.target === button) {
        if (inputUp.value === inputDown.value) {
            alert('You are welcome');
        } else {
            paragraph.textContent = 'Потрібно ввести однакові значення';
            paragraph.style.color = 'red';
            form.append(paragraph);
        }
    }
});